#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){
	ofSetFrameRate(60);
	screenWidth = ofGetScreenWidth();
	screenHeight = ofGetScreenHeight();
	ofSetWindowShape(screenWidth, screenHeight);
	ofSetWindowPosition(0, 0);
	ofBackground(000);
}

//--------------------------------------------------------------
void ofApp::update(){
}

//--------------------------------------------------------------
void ofApp::draw(){
}


//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	if (key == 'q') {
		ofExit();
	}
}
